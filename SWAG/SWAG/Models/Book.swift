//
//  Book.swift
//  SWAG
//
//  Created by ChihYing on 4/14/18.
//  Copyright © 2018 ChihYing. All rights reserved.
//

import Foundation

struct Book {
    let author: String
    let categories: String?
    let id: Int
    let lastCheckedOut: String?
    let lastCheckedOutBy: String?
    let publisher: String?
    let title: String
    let url: String
    
    
    init(dictionary: [String:AnyObject]) {
        author = dictionary[Constants.SWAGResponseKeys.author] as! String
        categories = dictionary[Constants.SWAGResponseKeys.categories] as? String
        id = dictionary[Constants.SWAGResponseKeys.id] as! Int
        lastCheckedOut = dictionary[Constants.SWAGResponseKeys.lastCheckedOut] as? String
        lastCheckedOutBy = dictionary[Constants.SWAGResponseKeys.lastCheckedOutBy] as? String
        publisher = dictionary[Constants.SWAGResponseKeys.publisher] as? String
        title = dictionary[Constants.SWAGResponseKeys.title] as! String
        url = dictionary[Constants.SWAGResponseKeys.url] as! String
    }
    
    
    static func booksFromResult(_ results: [[String:AnyObject]]) -> [Book] {
        
        var books = [Book]()
        
        for result in results {
            books.append(Book(dictionary: result))
        }
        return books
    }
    
    static func bookFromResult(_ result: [String:AnyObject]) -> Book {
        
        return Book(dictionary: result)
    }
    
}
