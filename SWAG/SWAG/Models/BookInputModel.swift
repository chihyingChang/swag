//
//  bookInputModel.swift
//  SWAG
//
//  Created by ChihYing on 4/13/18.
//  Copyright © 2018 ChihYing. All rights reserved.
//

import Foundation
import FacebookShare

// Codable combines the Encodable and Decodable protocols

struct PostBook: Codable  {
    let author: String
    let categories: String?
    let title: String
    let publisher: String?
}

struct PutBook: Codable {
    let lastCheckedOutBy: String
}




