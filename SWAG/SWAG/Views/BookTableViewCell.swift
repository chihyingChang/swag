//
//  BookTableViewCell.swift
//  SWAG
//
//  Created by ChihYing on 4/14/18.
//  Copyright © 2018 ChihYing. All rights reserved.
//

import UIKit

class BookTableViewCell: UITableViewCell {

    @IBOutlet weak var Title: UILabel!
    @IBOutlet weak var Author: UILabel!
    @IBOutlet weak var Publisher: UILabel!
    @IBOutlet weak var checkMark: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
