//
//  CheckOutView.swift
//  SWAG
//
//  Created by ChihYing on 4/15/18.
//  Copyright © 2018 ChihYing. All rights reserved.
//

import UIKit

class CheckOutView: UIView {
    
    @IBOutlet var contentView: UIView!
    
    @IBOutlet weak var checkOutButton: UIButton!
    @IBOutlet weak var categories: UILabel!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var author: UILabel!
    @IBOutlet weak var publisher: UILabel!
    @IBOutlet weak var lastCheckedOut: UILabel!
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setUp()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    private func setUp() {
        Bundle.main.loadNibNamed("CheckOutView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = bounds
    }

}
