//
//  Constants.swift
//  SWAG
//
//  Created by ChihYing on 4/13/18.
//  Copyright © 2018 ChihYing. All rights reserved.
//

import Foundation

struct Constants {
    
    struct SWAG {
        static let ApiScheme = "http"
        static let ApiHost = "prolific-interview.herokuapp.com"
        static let ApiPath = "/5ace1d3854009c0009e68f26/books"
        static let ApiClearPath = "/5ace1d3854009c0009e68f26/clean"
    }
    
    struct SWAGResponseKeys {
        static let author = "author"
        static let categories = "categories"
        static let id = "id"
        static let lastCheckedOut = "lastCheckedOut"
        static let lastCheckedOutBy = "lastCheckedOutBy"
        static let publisher = "publisher"
        static let title = "title"
        static let url = "url"
    }
    
}
