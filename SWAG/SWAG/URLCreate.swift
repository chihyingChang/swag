//
//  createURL.swift
//  SWAG
//
//  Created by ChihYing on 4/14/18.
//  Copyright © 2018 ChihYing. All rights reserved.
//

import Foundation

public func SWAGURLWithParameters(_ pathExtension: String? = nil) -> URL {
    
    var component = URLComponents()
    component.scheme = Constants.SWAG.ApiScheme
    component.host = Constants.SWAG.ApiHost
    component.path = Constants.SWAG.ApiPath + "/" + (pathExtension ?? "")
    return component.url!
}
