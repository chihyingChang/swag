//
//  ViewController.swift
//  SWAG
//
//  Created by ChihYing on 4/13/18.
//  Copyright © 2018 ChihYing. All rights reserved.
//

import UIKit

class BookListViewController: UIViewController {
    
    @IBOutlet weak var bookTableView: UITableView!
    
    var books = [Book]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        // Every time the Book List Page shows up, the books data will be updated
        getAllBooks();
        bookTableView.tableFooterView = UIView() // remove empty cells
    }
    
    private func getAllBooks() {
        
        // create session and request
        let session = URLSession.shared
        let request = URLRequest(url: SWAGURLWithParameters())
        
        let task = session.dataTask(with: request) { (data, response, error) in
            
            guard (error == nil) else {
                print("There was an error with the request: \(error!)")
                return
            }
            
            guard let statusCode = (response as? HTTPURLResponse)?.statusCode, statusCode >= 200 && statusCode <= 299 else {
                print("The request returned a status code other than 2xx!")
                return
            }
            
            guard let data = data else {
                print("No data was returned by the request!")
                return
            }
            
            // parse the data
            let parsedResults: [[String:AnyObject]]!
            do {
                parsedResults = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [[String:AnyObject]]
            } catch {
                print("Fail to parse the result!")
                return
            }
            
            // use the data
            self.books = Book.booksFromResult(parsedResults)
            
            // update UI
            performUIUpdatesOnMainThread {
                self.bookTableView.reloadData()
                self.hideTableVIew()
            }
            
        }
        task.resume()
    }
    
    @IBAction func deleteAll(_ sender: UIBarButtonItem) {
       displayCancelAlert(title: "Are you sure to delete all boooks?", message: "")
    }
    
    private func displayCancelAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action) in
            self.deleteAll()
        }))
        present(alert, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationViewController = segue.destination as? DetailsViewController {
            destinationViewController.books = books
            
            if let indexPath = bookTableView.indexPathForSelectedRow {
                destinationViewController.currentIndex = indexPath.row
            }
        }
    }
    
    private func deleteAll() {
        // create session and request
        let session = URLSession.shared
        
        var component = URLComponents()
        component.scheme = Constants.SWAG.ApiScheme
        component.host = Constants.SWAG.ApiHost
        component.path = Constants.SWAG.ApiClearPath
        
        var request = URLRequest(url: component.url!)
        request.httpMethod = "DELETE"
        
        // Make the request
        let task = session.dataTask(with: request) { (data, response, error) in
            
            guard (error == nil) else {
                print("There was an error with the request: \(error!)")
                return
            }
            
            guard let statusCode = (response as? HTTPURLResponse)?.statusCode, statusCode >= 200 && statusCode <= 299 else {
                print("The request returned a status code other than 2xx!")
                return
            }
            
            // update UI
            self.books = []
            
            performUIUpdatesOnMainThread {
                self.bookTableView.reloadData()
                self.hideTableVIew()
            }
        }
        task.resume()
    }
    
    private func hideTableVIew() {
        bookTableView.isHidden = self.books.count == 0 ? true : false
    }

}

extension BookListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return books.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "BookTableViewCell", for: indexPath) as! BookTableViewCell

        let book = books[indexPath.row]
        cell.Title.text = book.title;
        cell.Author.text = book.author;
        cell.Publisher.text = book.publisher != nil && !(book.publisher?.isEmpty)! ? "@ \(book.publisher!)" : "";
        cell.checkMark.isHidden = book.lastCheckedOutBy == nil
        
        return cell
    }
    
    // MARK: Swipe Setting
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            deleteOneBook(ID: books[indexPath.row].id)
            books.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
            
        }
    }
    
    private func deleteOneBook(ID: Int) {
        // create session and request
        let session = URLSession.shared
        var request = URLRequest(url: SWAGURLWithParameters("\(ID)"))
        request.httpMethod = "DELETE"
        
        // Make the request
        let task = session.dataTask(with: request) { (data, response, error) in
            
            guard (error == nil) else {
                print("There was an error with the request: \(error!)")
                return
            }
          
            guard let statusCode = (response as? HTTPURLResponse)?.statusCode, statusCode >= 200 && statusCode <= 299 else {
                print("The request returned a status code other than 2xx!")
                return
            }
            
        }
        task.resume()
    }
    
    
}
