//
//  AddViewController.swift
//  SWAG
//
//  Created by ChihYing on 4/13/18.
//  Copyright © 2018 ChihYing. All rights reserved.
//

import UIKit

class AddViewController: UIViewController {

    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var doneButton: UIButton!
    
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var authorTextField: UITextField!
    @IBOutlet weak var publisherTextField: UITextField!
    @IBOutlet weak var categoriesTextField: UITextField!
    
    var currentTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        backgroundView.layer.cornerRadius = 15
        submitButton.layer.cornerRadius = 8
        doneButton.layer.cornerRadius = 6
        
        setGestureRecognizer()
        subscribeToKeyboardNotifications()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        unsubscribeToKeyboardNotifications()
    }

    @IBAction func DoneAction(_ sender: Any) {
        guard (titleTextField.text?.isEmpty)! && (authorTextField.text?.isEmpty)! &&
            (categoriesTextField.text?.isEmpty)! && (publisherTextField.text?.isEmpty)! else {
            displayCancelAlert(title: "Are you sure to leave?", message: "The data will not be saved once you leave this page.")
            return
        }
        dismiss(animated: true, completion: nil)
    }

    @IBAction func SubmitAddBook(_ sender: UIButton) {
        
        if titleTextField.text == nil || (titleTextField.text?.isEmpty)! ||
            authorTextField.text == nil || (authorTextField.text?.isEmpty)! {
            setRedBorder()
            displayOKAlert(title: "Please fill out the required the fields." ,message: "Book title and author are required")
            return
        }
        
        let postBody = PostBook(author: authorTextField.text! , categories: categoriesTextField.text, title: titleTextField.text!, publisher: publisherTextField.text)
        
        // create session and request
        let session = URLSession.shared
        var request = URLRequest(url: SWAGURLWithParameters())
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        do {
            //encode postbook struct into JSON data
            let jsonData = try JSONEncoder().encode(postBody)
            request.httpBody = jsonData
            print(request)
        } catch (let error) {
            print(error)
        }
        
        // Make the request
        let task = session.dataTask(with: request) { (data, response, error) in
            
            guard (error == nil) else {
                print("There was an error with the request: \(error!)")
                return
            }
            
            guard let statusCode = (response as? HTTPURLResponse)?.statusCode, statusCode >= 200 && statusCode <= 299 else {
                print("The request returned a status code other than 2xx!")
                return
            }
            
            self.dismiss(animated: true, completion: nil)
        }
        task.resume()
    }
    
    private func displayOKAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        present(alert, animated: true)
    }
    
    private func displayCancelAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action) in
            self.dismiss(animated: true, completion: nil)
        }))
        present(alert, animated: true)
    }
    
    private func setRedBorder(){
    
        titleTextField.layer.borderWidth = (titleTextField.text?.isEmpty)! ? 1.0 : 0.0
        titleTextField.layer.borderColor = UIColor.red.cgColor
        authorTextField.layer.borderWidth = (authorTextField.text?.isEmpty)! ? 1.0 : 0.0
        authorTextField.layer.borderColor = UIColor.red.cgColor
        
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        guard currentTextField != nil else {
            return
        }
        let positionY = backgroundView.convert(currentTextField.frame.origin, to: view).y
        let textFieldHeight = currentTextField.frame.height
        let keyboardHeight = getKeyboardHeight(notification)
        if ( view.frame.height - keyboardHeight - 50 < positionY + textFieldHeight )  {
            
            UIView.animate(withDuration: 0.5, animations: {
                self.view.frame.origin.y = (self.view.frame.height - keyboardHeight - 50 ) - (positionY + textFieldHeight)
            })
        } else {
            keyboardWillHide(notification)
        }

    }
    
    @objc func keyboardWillHide(_ notification: Notification? = nil) {
        UIView.animate(withDuration: 0.5) {
            self.view.frame.origin.y = 0
        }
    }
    
    func getKeyboardHeight(_ notification: Notification) -> CGFloat {
        
        let userInfo = notification.userInfo
        let keyboardSize = userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue
        return keyboardSize.cgRectValue.height
    }
    
    func setGestureRecognizer() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapAction))
        tap.numberOfTapsRequired = 1
        view.addGestureRecognizer(tap)
    }
    
    @objc private func tapAction() {
        view.endEditing(true)
        keyboardWillHide(nil)
    }
    
    private func subscribeToKeyboardNotifications() {
    
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: .UIKeyboardDidHide, object: nil)
    }
    
    private func unsubscribeToKeyboardNotifications() {
        
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
    }
    
    
}

extension AddViewController : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if let text = textField.text,
            let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange,
                                                       with: string)
            if (textField.layer.borderWidth > 0.0 && updatedText.count > 0) {
                setRedBorder()
            }
        }
        return true
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        currentTextField = textField
    }
    
    
}
