//
//  DetailsViewController.swift
//  SWAG
//
//  Created by ChihYing on 4/13/18.
//  Copyright © 2018 ChihYing. All rights reserved.
//

import UIKit
import FacebookShare
import FBSDKCoreKit
import FBSDKLoginKit

class DetailsViewController: UIViewController {
    
    @IBOutlet weak var detailScrollView: UIScrollView!
    @IBOutlet weak var scrollViewTopConstraint: NSLayoutConstraint!
    
    var books : [Book]!
    var currentIndex : Int!
    var fullSize : CGSize!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setScrollView()
    }
    
    private func setScrollView() {
        
        fullSize = UIScreen.main.bounds.size
        detailScrollView.contentSize = CGSize(width: fullSize.width * CGFloat(books.count), height: fullSize.height - scrollViewTopConstraint.constant)
        detailScrollView.contentOffset = CGPoint(x: fullSize.width * CGFloat(currentIndex), y: 0.0)
        detailScrollView.isDirectionalLockEnabled = false
        
        let margin = CGFloat(40)
        var checkOutView : CheckOutView
        for i in 0...books.count - 1 {
            checkOutView = CheckOutView(frame: CGRect(x: 0.0, y: 0.0, width: fullSize.width - margin, height: fullSize.height - margin - scrollViewTopConstraint.constant ))
            checkOutView.center = CGPoint(x: fullSize.width * (0.5 + CGFloat(i)), y: (fullSize.height - margin - scrollViewTopConstraint.constant) * (0.5) )
            checkOutView.contentView.layer.cornerRadius = 13
            checkOutView.checkOutButton.addTarget(self, action: #selector(editBookInfo), for: UIControlEvents.touchUpInside)
            checkOutView.tag = i + 1  // page
            checkOutView.title.text = books[i].title
            checkOutView.categories.text = books[i].categories ?? ""
            checkOutView.author.text = books[i].author
            checkOutView.publisher.text = books[i].publisher != nil ? "Publisher: \(books[i].publisher!)" : ""
            checkOutView.lastCheckedOut.text = books[i].lastCheckedOutBy != nil ? "\(books[i].lastCheckedOutBy!) @ \(books[i].lastCheckedOut!)" : ""
            
            detailScrollView.addSubview(checkOutView)
        }
    }

    @objc private func editBookInfo() {
        let alert = UIAlertController(title: "Checkout", message: "Please enter your full name.", preferredStyle: .alert)
        
        let doneAction = UIAlertAction(title: "Done", style: .default) { (alertAction) in
            let textField = alert.textFields![0] as UITextField
            
            // check if the input is valid
            guard !(textField.text?.isEmpty)! else{
                return
            }
            // then update book with put request
            let put = PutBook(lastCheckedOutBy: textField.text!)
            self.updateOneBook(put: put)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alert.addTextField { (textField) in
            textField.placeholder = "Enter your name"
        }
        
        alert.addAction(doneAction)
        alert.addAction(cancelAction)
        present(alert, animated: true, completion: nil)
        
    }
    
    private func updateOneBook(put : PutBook) {
 
        let ID = books[self.currentIndex].id
        
        // create session and request
        let session = URLSession.shared
        var request = URLRequest(url: SWAGURLWithParameters("\(ID)"))
        request.httpMethod = "PUT"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        do {
            //encode postbook struct into JSON data
            let jsonData = try JSONEncoder().encode(put)
            request.httpBody = jsonData
        } catch {
            print(error)
        }
        
        // Make the request
        let task = session.dataTask(with: request) { (data, response, error) in
            
            guard (error == nil) else {
                print("There was an error with the request: \(error!)")
                return
            }
            
            guard let statusCode = (response as? HTTPURLResponse)?.statusCode, statusCode >= 200 && statusCode <= 299 else {
                print("The request returned a status code other than 2xx!")
                return
            }
            
            guard let data = data else {
                print("No data was returned by the request!")
                return
            }
            
            // parse the data
            let parsedResults: [String:AnyObject]!
            do {
                parsedResults = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String:AnyObject]
            } catch {
                return
            }
            
            // update view
            let book = Book.bookFromResult(parsedResults)
            self.books[self.currentIndex] = book
            
            performUIUpdatesOnMainThread {
                if let view = self.detailScrollView.viewWithTag(self.page()) as? CheckOutView{
                    view.lastCheckedOut.text = book.lastCheckedOutBy != nil ? "\(book.lastCheckedOutBy!) @ \(book.lastCheckedOut!)" : ""
                } else {
                    self.dismiss(animated: true, completion: nil)
                    
                }
            }
        }
        
        task.resume()
    }
    
    @IBAction func shareToFacebook(_ sender: UIButton) {
        
        let url = SWAGURLWithParameters("\(books[currentIndex].id)")
        
        let content = LinkShareContent(url: url, quote: "Reconmmend the book from Prolific Library!")
        
        let shareDialog = ShareDialog(content: content)
        
        shareDialog.presentingViewController = self
        
        do {
            try shareDialog.show()
        } catch (let error) {
            print("shareToFacebook \(error)")
        }
    }
    
    @IBAction func backToList(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }

    private func page()-> Int{
        return currentIndex + 1
    }
}

extension DetailsViewController : UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        currentIndex = Int(scrollView.contentOffset.x / fullSize.width)
    }
}
