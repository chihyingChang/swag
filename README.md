

## Title : The Prolific Library

---

## Overview

The application provides the SWAG committee with a way to track who has which book from the Prolific library. 

---

## Features

1. Get all book information from the remote storage
2. Add new books to the storage
3. Update the checkout record
4. Share the book information on social media

---

## Design Pattern 

I use MVC pattern for the application architecture.

1. Model : The model manages the application data and the parameters of the http requests, like Book.swift, BookInputModel,swift.
2. View Controller : The view controller responds to the user input and performs interactions on the data model objects, like BookListViewController, AddViewController and DetailsViewController.
3. View : The view presents the model in a particular format, such as BookTableViewCell, CheckoutView.

---

## Core classes/structs and functions

1. Constants struct is used to define the scheme, host, path and parameters of the APIs.
2. BookListViewController class is used to fetch the book data from the server and shows the list of all the books and their checked out marks. Also, users can delete any book by swiping the corresponding cell left and delete all books at once by tapping the trash icon in the top right.
3. AddViewController class supplies a few text fields to add data for a new book. Once the submit button is pressed, the new book will be added to the book storage.
4. DetailsViewController class presents the detailed information of each book, including title, author, tags, the latest book borrower etc. A user can checkout books here and scroll horizentally to see the deatils of the other books without returning to the list page. In addition, there is a share icon to access the Facebook sharing dialog after logging into an account. 
5. Book struct has the attributes of each book and functions to convert a JSON parsed result into the Book object(s).
6. URLCreate file provides a public function that is used to create a specific URL with or without a path extention.
7. GCDPerform file gives a public function to operate GCD to update the user interface when changes are made.

---


## How to Run

I use Swift 4, iOS 11 code and XCode Version 9.2 to develop the project.
The application can be run on different iPhone devices.

---
